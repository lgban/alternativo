defmodule Rottenpotatoes.Listing.Movie do
  use Ecto.Schema
  import Ecto.Changeset

  schema "movies" do
    field :description, :string
    field :rating, :string
    field :release_date, :date
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(movie, attrs) do
    movie
    |> cast(attrs, [:title, :description, :release_date, :rating])
    |> validate_required([:title, :release_date, :rating])
  end
end
