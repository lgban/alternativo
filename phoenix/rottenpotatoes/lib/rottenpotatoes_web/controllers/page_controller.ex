defmodule RottenpotatoesWeb.PageController do
  use RottenpotatoesWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
